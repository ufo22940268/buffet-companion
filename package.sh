#!/bin/bash
rm Buffet.zip
zip -r Buffet.zip . -x "*.git*" -x ".idea*" -x "asset/*" -x "*.zip" -x "package.sh"
