/**
 * Created by cc on 2020/3/19.
 */
'use strict';

let table = document.getElementById('main-table-body');
let emptyWarningElement = document.getElementById('empty-warning');
let changeDirButton = document.getElementById('change-dir-button');
let changeDirInput = document.getElementById('change-dir-input');
let changeDirSaveElement = document.getElementById('change-dir-save');
let changeDirIntro = document.getElementById('change-dir-instruction');
let currentDirElement = document.getElementById('code-root');
let currentDirSectionElement = document.getElementById('code-root-section');
let requestJsonElement = document.getElementById('request-json');
let responseJsonElement = document.getElementById('response-json');
let errorCodeElement = document.getElementById('error_code');
let errorCodeContentElement = document.getElementById('error_code_content');
let errorDetailContentElement = document.getElementById('error_detail_content');
errorCodeElement.hidden = true;

let clearRequests = function () {
  table.innerHTML = '';
  rows = [];
  cache = [];
};

document.getElementById('clear')
  .addEventListener('click', () => {
    clearRequests();
  });

function makeIntentUrl(url, type, line) {
  if (type === 'Webstorm') {
    return `webstorm://open?file=${url}&line=${line}`;
  } else if (type === 'VS Code') {
    return `vscode://file${url}:${line}`;
  }
}

function createActionButton(text, fileUrl, line) {
  let button = document.createElement('button');
  button.innerText = text;
  button.style.marginLeft = '10px';
  let intentUrl = makeIntentUrl(fileUrl, text, line);
  button.onclick = ev => {
    chrome.tabs.create({ url: intentUrl });
  };
  return button;
}

let codeRootDir;

chrome.storage.local.get(['codeRootDir'], (result) => {
  codeRootDir = result['codeRootDir'];
});

changeDirSaveElement.addEventListener('click', () => {
  let dir = changeDirInput.value;
  codeRootDir = dir;
  emptyWarningElement.hidden = false;
  showChangeRoot(false);
  currentDirSectionElement.hidden = true;
  chrome.storage.local.set({ codeRootDir: dir });
});

let showChangeRoot = function (show) {
  let buttons = [changeDirInput, changeDirSaveElement, changeDirIntro];
  if (show) {
    buttons.forEach(b => b.removeAttribute('hidden'));
  } else {
    buttons.forEach(b => b.hidden = true);
  }
};
changeDirButton.addEventListener('click', () => {
  showChangeRoot(true);
});


changeDirInput.addEventListener('input', function (t) {
  clearRequests();
});

function extractRootDir(fileUrl) {
  return fileUrl.match(/(^.+)(?=\/(dashboard_)?app\/)/)[1];
}

let cache = [];
let rows = [];

function insertNewSection(referer) {
  let row = table.insertRow(-1);
  let element = row.insertCell(0);
  element.setAttribute('height', '45px');
  element.setAttribute('colspan', '2');
  element.style.fontStyle = 'bold';
  element.style.fontSize = 'x-large';
  element.style.color = 'tomato';
  element.innerHTML = referer;
}

function insertNewRow(data) {
  let { fileUrl, line } = data;

  let lastData = cache.slice(-1)[0];
  if (!lastData || lastData.referer !== data.referer) {
    insertNewSection(data.referer);
  }
  cache.push(data);

  let displayCodeRoot = codeRootDir;
  if (codeRootDir) {
    fileUrl = fileUrl && fileUrl.replace(/(^.+)(?=\/(dashboard_)?app\/)/, codeRootDir);
    displayCodeRoot = codeRootDir;
  } else {
    displayCodeRoot = extractRootDir(fileUrl);
  }

  const endPointPath = fileUrl.match(/(^.+)(\/(dashboard_)?app\/.+)$/)[2];
  changeDirInput.setAttribute('placeholder', displayCodeRoot);
  currentDirSectionElement.hidden = false;
  currentDirElement.innerText = displayCodeRoot;

  emptyWarningElement.setAttribute('hidden', 'true');

  const row = table.insertRow(-1);
  rows.push(row);
  row.className = 'row';
  const rowCell = row.insertCell(0);
  let addColumn = (width) => {
    let element = document.createElement('div');
    element.style.display = 'inline-block';
    element.style.width = width;
    rowCell.appendChild(element);
    return element;
  };
  const timeCell = addColumn('10%');
  const requestTimeCell = addColumn('10%');
  requestTimeCell.id = 'request-time';
  const contentCell = addColumn('60%');
  contentCell.id = 'url-content';
  const buttonsCell = addColumn('20%');
  buttonsCell.style.textAlign = 'right';
  timeCell.innerHTML = new Date().toLocaleTimeString();
  contentCell.innerHTML = `${endPointPath}:${line}`;

  buttonsCell.appendChild(createActionButton('Webstorm', fileUrl, line));
  buttonsCell.appendChild(createActionButton('VS Code', fileUrl, line));

  row.addEventListener('click', function (ev) {
    selectRow(this);
  });

  window.scrollTo(0, document.body.scrollHeight);
}

let newData = {
  time: new Date().toLocaleTimeString(), fileUrl: '/a/app/c', line: 3, referer: 'www.baidu.com'
};

let insideChromeExtensionEnvironment = function () {
  return window.chrome && chrome.runtime && chrome.runtime.id;
};

function showDetail(row) {
  let index = rows.indexOf(row);
  let data = cache[index];
  let payload = JSON.parse(data.payload);
  requestJsonElement.innerText = JSON.stringify(payload, null, 2);
  let response;
  try {
    response = JSON.parse(data.content);
  } catch (e) {
    response = data.content;
  }
  responseJsonElement.innerText = JSON.stringify(response, null, 2);

  errorCodeElement.hidden = !data['errorCode'];
  errorCodeContentElement.innerText = data['errorCode'];
  errorDetailContentElement.innerText = formatJson(data['errorDetail']);
}

function formatJson(obj) {
  try {
    return JSON.stringify(JSON.parse(obj), null, 2);
  } catch (e) {
    return obj;
  }
}

let selectRow = function (row) {
  for (let i = 0; i < rows.length; i++) {
    let r = rows[i];
    if (r === row) {
      r.classList.add('selected-row');
    } else {
      r.classList.remove('selected-row');
    }
  }
  showDetail(row);
};

if (!insideChromeExtensionEnvironment()) {
  insertNewRow(newData);
  insertNewRow(newData);
  insertNewRow(newData);
  selectRow(rows[0]);
}

function updateResponse(message) {
  //It's an content message
  let index = cache.findIndex(t => t.id === message.id);
  let data = cache[index];
  let row = rows[index];
  if (data) {
    data['errorCode'] = message.errorCode;
    data['errorDetail'] = message.errorDetail;
    data['content'] = message.content;
    data['time'] = message.time;
    let requestTimeElement = row.querySelector('#request-time');
    requestTimeElement.innerText = message.time ? message.time / 1000 + 's' : '';
    if (message.time > 3000) {
      requestTimeElement.className = 'long-long-time';
    } else if (message.time > 1000) {
      requestTimeElement.className = 'long-time';
    } else {
      requestTimeElement.className = 'normal-time';
    }

    if (data['errorCode']) {
      let contentUrlElement = row.querySelector('#url-content');
      contentUrlElement.innerHTML = contentUrlElement.innerHTML + `<span style="
          margin-left: 20px;
        color: red;
        font-weight: bold"/>Error<span>`;
    }
  }
}

function filterLocation(loc) {
  return !/msgbox.js:\d+:\d+$/.test(loc);
}

chrome.devtools.network.onRequestFinished.addListener(request => {
  let loc = (
    request.request.headers.find(h => h.name === 'x-buffet-code-loc') || {}
  ).value;

  let referer = (
    request.request.headers.find(h => h.name === 'Referer') || {}
  ).value;

  if (loc && filterLocation(loc)) {
    let line = loc.match(/:(\d+):\d+$/)[1];
    loc = loc.replace(/:\d+:\d+$/, '');
    let fileUrl = `${loc}`;
    let payLoad = '';
    if (request.request.postData) {
      payLoad = request.request.postData.text;
    }

    let id = new Date().getTime();
    let c = { id: id, fileUrl: fileUrl, line: line, source: 'buffet-companion', referer: referer, payload: payLoad };
    insertNewRow(c);


    request.getContent((c) => {
      let errorCode;
      let errorDetail;
      let time = Math.floor(request.time);
      let s = c;
      if (request.response && request.response.headers) {
        let t1 = request.response.headers.find(h => h.name === 'x-buffet-error-code');
        let ed = request.response.headers.find(h => h.name === 'x-buffet-error-detail');
        errorCode = (t1 && t1.value) || '';
        errorDetail = ed && ed.value;
      }

      let message = {
        id: id, content: s, errorCode: errorCode, errorDetail: errorDetail, source: 'buffet-companion', time: time
      };
      updateResponse(message);
    });
  }
});
